package valiit.day02;

import java.math.BigDecimal;
import java.math.BigInteger;

public class Day02Exercises {
    public static void main(String[] args) {

        // [lesanne 1
        double a = 456.78;
        String[] jada = {"t", "e", "s", "t"};
        System.out.println(jada[0]);

        boolean isTrue = true;
        String b = "a";
        char c = 'a';

        int[] d = {5, 91, 304, 405};

        double[] f = {56.7, 45.8, 91.2};

        String[] g = {"see on esimene väärtus", "67", "58.92"};
        Object[] kogum = {"see on esimene väärtus", 67, 58.92};

        double h = 7676868683452352345324534534523453245234523452345234523452345D;
        BigInteger myVeryLargeInteger =
                new BigInteger("7676868683452352345324534534523453245234523452345234523452345");

        BigDecimal myVeryLargeDecimal =
                new BigDecimal("7676868683452352345324534534523453245234523452345234523452345.5646546546");

    }
}