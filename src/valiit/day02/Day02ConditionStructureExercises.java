package valiit.day02;

public class Day02ConditionStructureExercises {
    public static void main(String[] args) {
        // [lesanne 2

        String linn = "Berlin";

        if (linn.equals("Milano")) {
            System.out.println("Ilm on soe.");
        } else {
            System.out.println("Ilm polegi kõige tähtsam.");
        }

        // ülesanne 3

        int yksKuniViis = 5;

        if (yksKuniViis == 1) {
            System.out.println("nõrk");
        } else if (yksKuniViis == 2) {
            System.out.println("mitterahuldav");
        } else if (yksKuniViis == 3) {
            System.out.println("rahuldav");
        } else if (yksKuniViis == 4) {
            System.out.println("hea");
        } else if (yksKuniViis == 5) {
            System.out.println("väga hea");
        }

        // ülesanne 4

        int grade = 6;

        switch(grade) {
            case 1:
                System.out.println("nõrk");
                break;
            case 2:
                System.out.println("mitterahuldav");
                break;
            case 3:
                System.out.println("rahuldav");
                break;
            case 4:
                System.out.println("hea");
                break;
            case 5:
                System.out.println("väga hea");
                break;
            default:
                System.out.println("Error!");
        }

        // ex 5
        int vanus = 100;

        String onVana = vanus < 100 ? "Noor" : "Vana";
        System.out.println(onVana);

        // ex
        String ageDescription = vanus < 100 ? "Noor" : vanus == 100 ? "Peaaegu vana" : "Vana";
        System.out.println(ageDescription);


    }
}
