package valiit.day01;

public class DataTypeDemo {
    public static void main(String[] args) {

        // kompilaator ei näe
        /*
        Seda ka mitte,
        üldseeinäe
         */

        // Andmetüübid

        // 1 - byte (1 bait)
        byte mySmallNumber = 127; //
        System.out.println(mySmallNumber);

        System.out.print("\n"); //lisarida

        // 2 - short (2 baiti)
        short myMediumSizedNumber = 11000;
        System.out.println(myMediumSizedNumber);

        // 3 - int (4 baiti)
        int myRelativelyLargeNumber = 1000000000;
        myRelativelyLargeNumber = 1_000_000_000; // mugavus, et numbrit saaks lugeda kui suur number
        System.out.println(myRelativelyLargeNumber);

        // 4 - long (8 baiti)
        long myVeryLargeNumber = 200_000_000_000_000_000L; // L tähendab lõpus, et tegu ikka longiga, et intellij ei viriseks
        System.out.println(myVeryLargeNumber);

        // ----------------------------------------------------------------

        // 5 - float (4 baiti). Floati saab panna ka komakohaga arve
        float myDecimal1 = 4.8F; // F viide sellele, et tegemist floatiga.
        float myDecimal2 = 56.4F;
        float myDecimal3 = myDecimal1 + myDecimal2;
        /* Float üritab teatud määral ümardada ja pole nii täpne väga suurte või väga väikeste arvude puhul */
        System.out.println(myDecimal3);

        // 6 - double (8 baiti)
        double myDecimal4 = 4.8D; // F viide sellele, et tegemist floatiga.
        double myDecimal5 = 56.4D;
        double myDecimal6 = myDecimal4 + myDecimal5;
        System.out.println(myDecimal6);

        // 7 - char (2 baiti) - mittenumbriline, kaks tähte, igal sümbolil on oma numbrikood
        char myChar1 = 's';
        short myChar3 = 116;
        System.out.println((char)myChar3);

        char myChar2 = '2';
        System.out.println((short)myChar2);

        // 8 - boolean (1 bait)
        boolean isEarthRound = true || false && true;
        System.out.println(isEarthRound);

        // Wrapper classes
        Long wrapperA = 5L;
        Long wrapperB = 8L;
        long wrapperSum = wrapperA + wrapperB;
        System.out.println(wrapperSum);
    }
}
