package valiit.day01;

public class TextProcessingExercises {
    public static void main(String[] args) {

        // ex 1
        System.out.println("Hello, World");
        System.out.println("Hello \"World\"!");
        System.out.println("Hawking once said: \"Life would be tragic if it weren't funny\"");
        System.out.println("Kui liita kokku sõned \"See on teksti esimene pool  \" ning \"See on teksti teine pool\", siis tulemuseks saame \"See on teksti esimene pool See on teksti teine pool\".");
        System.out.println("Elu on ilus.");
        System.out.println("Elu on 'ilus'.");
        System.out.println("Elu on \"ilus\".");
        System.out.println("Kõige rohkem segadust tekitab \"-märgi kasutamine sõne sees.");
        System.out.println("Eesti keele kõige ilusam lause on: \"Sõida tasa üle silla!\"");
        System.out.println("'Kolm' - kolm, 'neli' - neli, \"viis\" - viis.");
        System.out.println("");

        // ex 2

        String tlnPop = "450 000";
        String popText1 = "Tallinnas elab " + tlnPop + " inimest.";

        String popText2 = String.format("Tallinnas elab %s inimest", tlnPop);
        System.out.println(popText2);

        int populationOfTallinn = 450_000;
        String popText3 = String.format("Tallinnas elab %,d inimest!!!", populationOfTallinn); // d nagu digit, koma seal sest Ameeriklased jms kasutavad t[hiku asemel koma
        System.out.println(popText3);

        // ex 3
        String bookTitle = "Rehepapp";
        System.out.println("Raamatu \"" + bookTitle + "\" autor on Andrus Kivirähk");
        String bookText = String.format("Raamatu \"%s\" autor onAndrus Kivirähk", bookTitle);


        // ex 4
        String planet1 = "Merkuur";
        String planet2 = "Veenus";
        String planet3 = "Maa";
        String planet4 = "Marss";
        String planet5 = "Jupiter";
        String planet6 = "Saturn";
        String planet7 = "Uraan";
        String planet8 = "Neptuun";
        int planetCount = 8;

        System.out.println(planet1 + ", " + planet2 + ", " + planet3 + ", " + planet4 + ", " + planet5 + ", " + planet6 + ", " + planet7 + " ja " + planet8 + " on Päikesesüsteemi " + planetCount + " planeeti");
        String planetText = String.format("%s, %s, %s, %s, %s, %s, %s ja %s on Päikesesüsteemi %d planeeti.", planet1, planet2, planet3, planet4, planet5, planet6, planet7, planet8, planetCount);
        System.out.println(planetText);
    }
}
