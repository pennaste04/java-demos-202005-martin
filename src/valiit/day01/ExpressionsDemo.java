package valiit.day01;

public class ExpressionsDemo {
    public static void main(String[] args) {

        // Arithmetic Expressions
        int a = 1;
        int b = 2;
        int c = a + b - 3 * b;
        System.out.println(c);

        a++;
        System.out.println(a);

        a--;
        System.out.println(a);

        int d = ++a; // a v''rtus suureneb, sest suurendamine tehakse enne, kui ++ on enne on ta k]rgema prioriteediga
        int d2 = a++; // operaatori prioriteet muutub, see on madalam kui omnistamine d = a
        System.out.println(d);
        System.out.println(d2);

        int e = 12;
//        e = e + 3;
        e += 3; // shortcut
//        e = e + 3;
        e -= 3;
//        e = e * 3;
        e *= 3;
        System.out.println(e);

        // jääk
        int f = 73;
        f = f % 10;
        System.out.println(f);

        // Conditional Expressions
        // = omistamisoperaator
        // == v]rdsuse kontrolli operaator
        boolean isCorrect = 5 != 6;
        System.out.println(isCorrect);
    }
}
