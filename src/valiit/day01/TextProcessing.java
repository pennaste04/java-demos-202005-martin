package valiit.day01;


public class TextProcessing {
    public static void main(String[] args) {

        // Stringide defineerimin
        String text1 = "tere";
        String text2 = new String("tere");

        /// Stringide kokkuliitmine
        String text3 = text1 + text2;
        System.out.println("text3: " + text3);
        String text4 = text1.concat(text2);
        System.out.println("text4: " + text4);
        text4 = text3;
        System.out.println(text3 == text4); // nii ei saa v]rrelda, sest v]rdleme tegelt malupiirkonna aadressi

        // Stringe v]rreldakse nii...
        System.out.println(".equals() " + text3.equals(text4));
        System.out.println(".equalsIgnoreCase() " + text3.equalsIgnoreCase(text4));

        String text5 = "Isa ütles: \t \"Tule \nsiia!\"";
        System.out.println(text5);

        System.out.println("Minu salajane fail: C:\\secret\\document.txt");

    }
}
