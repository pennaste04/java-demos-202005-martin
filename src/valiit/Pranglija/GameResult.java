package valiit.Pranglija;

public class GameResult {
    // Konteiner kus on sellised sahtlid / instruktsioon
    public String name;
    public double time;
    public String numberRange;

    @Override
    public String toString() {
        return "GameResult{" +
                "name='" + name + '\'' +
                ", time=" + time +
                ", numberRange='" + numberRange + '\'' +
                '}';
    }
}
