package valiit.Pranglija;

import java.io.IOException;
import java.lang.reflect.GenericArrayType;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PranglijaGame {
    private static final int GUESS_COUNT = 2;
//    private static List<String> gameLog = new ArrayList<>(); // Batman, 34,567s, 45 - 55
    private static List<GameResult> gameLog = new ArrayList<>();
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {



        System.out.println("---------------------------------");
        System.out.println("-------- PRANGLIJA-GAME ---------");
        System.out.println("---------------------------------");

        // Mäng...
        // 01.01.1970 00:00:00 - ajaarvamise aeg IT's

        while (true) {

            System.out.print("Sisesta oma nimi: ");
            String playerName = scanner.nextLine();

            System.out.print("Sisesta numbrivahemiku miinimum: ");
            int rangeMin = getIntFromConsole();

            System.out.print("Sisesta numbrivahemiku maksimum: ");
            int rangeMax = getIntFromConsole();

            long startTime = System.currentTimeMillis(); // Millisekundid

            String range = "";

            int count = 0;
            for (; count < GUESS_COUNT; count++) {
                int number1 = (int) (Math.random() * (rangeMax - rangeMin + 1)) + rangeMin;
                int number2 = (int) (Math.random() * (rangeMax - rangeMin + 1)) + rangeMin;

                System.out.print(String.format("%d + %d = ", number1, number2));
                int userGuess = Integer.parseInt(scanner.nextLine());
                System.out.println("Kasutaja arvas: " + userGuess);
                if ((number1 + number2) != userGuess) {
                    // Kasutaja eksis liitmisel...
                    break;
                }
                range = number1 + " - " + number2;
            }

            if (count == GUESS_COUNT) {
                System.out.println("Tubli! Sa võitsid!");

                long endTime = System.currentTimeMillis(); //
                double timeElapsed = endTime - startTime;
                timeElapsed = timeElapsed / 1000.0;
                System.out.println("Mäng kestis " + timeElapsed + " sekundit.");

                // Logime mängu gameLog muutujasse...
                // Tekitame objekti mängu tulemuse hoidmiseks (instruktsiooniks GameResult)
                GameResult result = new GameResult();
                result.name = playerName;
                result.time = timeElapsed;
                result.numberRange = rangeMin + " - " + rangeMax;
                gameLog.add(result);
                gameLog.sort((resultA, resultB) -> {
                    if (resultA.time > resultB.time) {
                        return 1; // indikatsioon, vaheta resultA ja resultB järjekord ära
                    }else{
                        return -1; // järjekord juba ongi õige, ära muuda järjekorda
                    }
                });

//                gameLog.add(String.format("%s, %f sekundit, %d - %d", playerName, timeElapsed, rangeMin, rangeMax));

                // Kuvame tulemuse...
                System.out.println("MÄNGU LOGI");
                for (GameResult gameResult : gameLog) {
                    System.out.println("-------------------------------");
                    System.out.println("Nimi: \t\t\t" + gameResult.name);
                    System.out.println("Aeg: \t\t\t" + gameResult.time + "s");
                    System.out.println("Numbrivahemik: \t\t" + gameResult.numberRange);
                    System.out.println();
                }

            } else {
                System.out.println("Loser!");
            }

            System.out.println("Mäng lõppes, mida soovid edasi teha?\n" +
                    "Mängi edasi\t - 1\n" +
                    "Lõpeta mäng\t - 0\n\n" +
                    "Sisesta valik: ");
            int userCommand = Integer.parseInt(scanner.nextLine());
            if (userCommand == 0) {
                break;
            }
        }

//        System.out.println(number1);
//        System.out.println(number2);




        System.out.println("---------------------------------");
        System.out.println("--------- Copyright: MP ---------");
        System.out.println("---------------------------------");

    }




    private static int getIntFromConsole() {
        while(true) {
            try {
                return Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.out.println("See tekst pole number.");
                System.out.print("Sisesta number: ");
            }
        }
    }


}
