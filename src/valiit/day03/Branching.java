package valiit.day03;

public class Branching {
    public static void main(String[] args) {

        boolean isValid = true;

        if(isValid) {

        }


        int i = 8;

        //Tingimustehte (inline-if statement)
        String canBeDividedByThree = i % 3 == 0 ? "jagub kolmega" : "ei jagu kolmega";
        System.out.println(canBeDividedByThree);

        // Klassikaline if-esle statement
        if (i % 3 == 0) {
            canBeDividedByThree = "jagub kolmega";
        } else {
            canBeDividedByThree = "ei jagu kolmega";
        }
        System.out.println(canBeDividedByThree);

        //Switch statement
        // Kasulik kasutada siis kui
        // a) kui valikuvariante on kindel kogus (ei ole lõpmatu)
        // b) kui valikuvariante on palju

        String trafficLightColor = "red";

        switch (trafficLightColor) {
            case "red":
                System.out.println("Cars must stop and wait.");
                break;
            case "yellow":
                System.out.println("Slow down");
                break;
            case "green":
                System.out.println("Drive!");
                break;
            default:
                System.out.println("Lights broken, madness!");
        }

        int tmp = 3;

        // allolev on uuemates Javades
//        String myText = switch(tmp) {
//            3 -> "kolm";
//            4 -> "neli";
//        }

    }
}
