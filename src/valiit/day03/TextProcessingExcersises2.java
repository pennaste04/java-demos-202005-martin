package valiit.day03;

import java.util.Scanner;

public class TextProcessingExcersises2 {
    public static void main(String[] args) {

        // ex 1
        String president1 = "Konstantin Päts";
        String president2 = "Lennart Meri";
        String president3 = "Arnold Rüütel";
        String president4 = "Toomas Hendrik Ilves";
        String president5 = "Kersti Kaljulaid";
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(president1);
        stringBuilder.append(", ");
        stringBuilder.append(president2);
        stringBuilder.append(", ");
        stringBuilder.append(president2);
        stringBuilder.append(", ");
        stringBuilder.append(president3);
        stringBuilder.append(", ");
        stringBuilder.append(president4);
        stringBuilder.append(" ja ");
        stringBuilder.append(president5);
        stringBuilder.append(" on Eesti presidendid.");

        System.out.println(stringBuilder);

        // ex 2
        String text1 = "Rida: See on esimene rida. Rida: See on teine rida. Rida: See on kolmas rida";

        Scanner myScanner = new Scanner(text1);
        myScanner.useDelimiter("Rida: ");
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());
        System.out.println(myScanner.next());

    }
}
