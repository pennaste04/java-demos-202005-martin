package valiit.day03;

public class ArrayDemo {
    public static void main(String[] args) {

        // Inline-meetodil kahetasandiline massiiv
        int[][] monthlyTemperatures = {
                {-7, 3},
                {-9, 7},
                {0, 5},
                {5, 12},
                {13, 15},
                {7, 18},  // juunikuu
                {16, 23},
                {16, 23},
                {8, 11},
                {5, 8},
                {3, 7},  // november
                {4, 1},
        };

        System.out.println("Juunikuu maksimaalne temperatuur: " + monthlyTemperatures[5][1]);
        System.out.println("Novembriikuu minimaalne temperatuur: " + monthlyTemperatures[10][0]);

        // "Klassikalisel viisil" kahetasandilise massiivi loomin

        int[][] monthlyTemperatures2 = new int[12][2];

        //Jaanuar
        monthlyTemperatures2[0][0] = -7;
        monthlyTemperatures2[0][1] = 3;

        //Veebruar
        monthlyTemperatures2[1][0] = -9;
        monthlyTemperatures2[1][1] = 7;

        //Märts
        monthlyTemperatures2[2][0] = 0;
        monthlyTemperatures2[2][1] = 5;

//        System.out.println("Juunikuu maksimaalne temperatuur: " + monthlyTemperatures2[5][1]);
//        System.out.println("Novembriikuu minimaalne temperatuur: " + monthlyTemperatures2[10][0]);

    }
}
