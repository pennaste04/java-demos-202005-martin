package valiit.day03;

public class ArrayExercises {
    public static void main(String[] args) {

        // ex 7
        int[] numbriMassiiv = new int[5];

        numbriMassiiv[0] = 1;
        numbriMassiiv[1] = 2;
        numbriMassiiv[2] = 3;
        numbriMassiiv[3] = 4;
        numbriMassiiv[4] = 5;

        System.out.println(numbriMassiiv[0]);
        System.out.println(numbriMassiiv[2]);
        System.out.println(numbriMassiiv[4]);

        // ex 8
        String[] myCities = {"Tallinn", "Helsinki", "Madrid", "Paris"};

        System.out.println(myCities[1]);

        // ex 9
        int[][] myNumbers = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9, 0},
        };

        int[][] matrix2 = new int[3][];
        matrix2[0] = new int[3];
        matrix2[0][0] = 1;
        matrix2[0][1] = 2;
        matrix2[0][2] = 3;
        matrix2[1] = new int[3];
        matrix2[1][0] = 4;
        matrix2[1][1] = 5;
        matrix2[1][2] = 6;
        matrix2[2] = new int[]{7, 8, 9, 0};

        System.out.println(myNumbers[2][3]);
        System.out.println(matrix2[2][3]);


        // ex 10
        String[][] countriesCities = {
                {"Tallinn", "Tartu", "Valga", "Võru"},
                {"Stockholm", "Uppsala", "Valga", "Võru"},
                {"Helsinki", "Espoo", "Hanko", "Jämsa"},
        };

        System.out.println(countriesCities[2][2]);

        String[][] countriesCities2 = new String[3][4];
        countriesCities[0][0] = "Tallinn";
        countriesCities[0][1] = "Tartu";
        countriesCities[0][2] = "Valga";
        countriesCities[0][3] = "Võru";
        countriesCities2[1][0] = "Stockholm";
        countriesCities2[1][1] = "Uppsala";
        countriesCities2[1][2] = "Lund";
        countriesCities2[1][3] = "Köping";
        countriesCities2[2][0] = "Helsinki";
        countriesCities2[2][1] = "Espoo";
        countriesCities2[2][2] = "Hanko";
        countriesCities2[2][3] = "Jämsa";

        System.out.println(countriesCities2[2][2]);

    }
}
