package valiit.day05;

import java.util.*;

public class Excercises {
    public static void main(String[] args) {

        // Ex 1
        // Kahe teineteise sees oleva for tsükli ülesanne

        for (int row = 0; row < 6; row++) {
            for (int col = 6; col > row; col--) {
                System.out.print("#");
            }
            System.out.println();
        }

        // Ex 2
        for (int row = 0; row < 6; row++) {
            for (int col = 0; col <= row; col++) {
                System.out.print("#");
            }
            System.out.println();
        }

        // Ex 3
        for (int row = 0; row < 5; row++) {
            int col = 4;
            for (; col > row; col--) {
                System.out.print(" ");
            }
            for (int num = col + 1; num > 0; num--) {
                System.out.print("@");
            }
            System.out.println();
        }

        // alternatiiv
        for (int row = 1; row <= 5; row++) {
            for (int col = 1; col <= row; col++) {
                System.out.print(col <= 5 - row ? " " : "@");
            }
            System.out.println();
        }

        // Ex 4

        int number = 1234567;
        String numberString = String.valueOf(number);
        String reversedNumberString = "";
        for (char c : numberString.toCharArray()) {
            reversedNumberString = c + reversedNumberString;
        }
        int result = Integer.parseInt(reversedNumberString);
        System.out.println(result);

        // variant 2 Stringbuilder
        number = 389;
        numberString = String.valueOf(number);
        StringBuilder stringBuilder = new StringBuilder(numberString);
        reversedNumberString = stringBuilder.reverse().toString();
        result = Integer.parseInt(reversedNumberString);
        System.out.println(result);


        // Ex 5
        String studentName = args[0];

//        for (int i = 0; i < args.length; i++)
        int studentScore = Integer.parseInt(args[1]);
        int grade = 0;

        if (studentScore < 51) {
            grade = 0;
        } else if (studentScore < 61) {
            grade = 1;
        } else if (studentScore < 71) {
            grade = 2;
        } else if (studentScore < 81) {
            grade = 3;
        } else if (studentScore < 91) {
            grade = 4;
        } else if (studentScore < 101) {
            grade = 5;
        } else {
            grade = -1;
        }

        if (grade == 0) {
            System.out.println(String.format("%s: FAIL", studentName));
        } else if (grade > 0) {
            System.out.println(String.format("%s: PASS - %d, %d", studentName, grade, studentScore));
        } else {
            System.out.println("Ebakorrektne sisend");
        }


        // Ex 6
        double[][] triangleSidesSet = {
                {56.91, 45, 39},
                {23.65, 12.11},
                {98.99, 78.65},
                {123.61, 896.23},
                {456.172, 654.546},
                {349.0, 672.7},
                {3, 4}
        };

        for (int i = 0; i < triangleSidesSet.length; i++) {
            double sideA = triangleSidesSet[i][0];
            double sideB = triangleSidesSet[i][1];

            double sideC = Math.sqrt(Math.pow(sideA, 2) + Math.pow(sideB, 2));
            System.out.println(String.format("Täisnurkne kolmnurk küljepikkustega %.2f ja %.2f " +
                    "omab hüpotenuusi pikkusega %.2f.", sideA, sideB, sideC));
        }

        // Ex 7
//        Estonia, Tallinn, Jüri Ratas
//        Latvia, Riga, Arturs Krišjānis Kariņš
//        Lithuania, Vilnius, Saulius Skvernelis
//        Finland, Helsinki, Sanna Marin
//        Sweden, Stockholm, Stefan Löfven
//        Norway, Oslo, Erna Solberg
//        Denmark, Copenhagen, Mette Frederiksen
//        Russia, Moscow, Mikhail Mishustin
//        Germany, Berlin, Angela Merkel
//        France, Paris, Édouard Philippe

        String[][] countries = {
                {"Estonia", "Tallinn", "Jüri Ratas"},
                {"Latvia", "Riga", "Arturs Krišjānis Kariņš"},
                {"Lithuania", "Vilnius", "Saulius Skvernelis"},
                {"Finland", "Helsinki", "Sanna Marin"},
                {"Sweden", "Stockholm", "Stefan Löfven"},
                {"Norway", "Oslo", "Erna Solberg"},
                {"Denmark", "Copenhagen", "Mette Frederiksen"},
                {"Russia", "Moscow", "Mikhail Mishustin"},
                {"Germany", "Berlin", "Angela Merkel"},
                {"France", "Paris", "Édouard Philippe"}
        };

//        for (int i = 0; i < countries.length; i++) {
//            System.out.println(countries[i][2]);
//        }

        // Ex 8

        for (String[] country : countries) {
            System.out.println(String.format("Country: %s, Capital: %s, Prime minister: %s", country[0], country[1], country[2]));
        }


        String[][][] countries2 = {
                {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Latvian", "Finnish"}},
                {{"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}},
                {{"Lithuania}"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Lithuanian", "Russian"}},
                {{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Finnish", "Estonian"}},
                {{"Sweden"}, {"Stockholm"}, {"Stefan Löfven"}, {"Swedish", "Finnish", "English", "German"}},
                {{"Norway"}, {"Oslo"}, {"Erna Solberg"}, {"Norwegian", "Swedish"}},
                {{"Denmark"}, {"Copenhagen"}, {"Mette Frederiksen"}, {"Danish", "Swedish"}},
                {{"Russia"}, {"Moscow"}, {"Mikhail Mishustin"}, {"Russian", "Estonian"}},
                {{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"German", "English"}},
                {{"France"}, {"Paris"}, {"Édouard Philippe"}, {"Bonjour", "Celavie"}}
        };

        for (String[][] country : countries2) {

            System.out.println(String.format("%s / %s / %s:", country[0][0], country[1][0], country[2][0]));

            String[] languages = country[3];
            for (String lang : languages) {

                System.out.println("\t\t" + lang);
            }

        }


        // Ex 9

        List<List<List<String>>> countries3 =
                Arrays.asList( // riigid
                        Arrays.asList( // riik
                                Collections.singletonList("Eesti"), // riigi nimi
                                Collections.singletonList("Tallinn"), // pealinn
                                Collections.singletonList("Jüri Ratas"), // peaminister
                                Arrays.asList("Estonian", "Latvian", "Finnish")  // keeled
                        ),
                        Arrays.asList( // riik
                                Collections.singletonList("Läti"), // riigi nimi
                                Collections.singletonList("Riia"), // pealinn
                                Collections.singletonList("Kesiganes Karins Misiganes"), // peaminister
                                Arrays.asList("Latvian", "Russian", "Estonian")  // keeled
                        ),
                        Arrays.asList( // riik
                                Collections.singletonList("Leedu"), // riigi nimi
                                Collections.singletonList("Vilnius"), // pealinn
                                Collections.singletonList("Saulis Skvernelis"), // peaminister
                                Arrays.asList("Lithuanian", "Latvian", "English", "Russain")  // keeled
                        )
                );

        for (int i = 0; i < countries3.size(); i++) {
//            System.out.print(String.format());

            String countryName = countries3.get(i).get(0).get(0);
            String countryCapital = countries3.get(i).get(1).get(0);
            String countryPrimeMinister = countries3.get(i).get(3).get(0);
            List<String> countryLanguages = countries3.get(i).get(3);

            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (int j = 0; j < countryLanguages.size(); j++) {
                System.out.println("\t\t" + countryLanguages.get(j));
            }

        }
        System.out.println("-----");

        // Ex 10

        Map<String, String[][]> countries4 = new TreeMap<>();
        countries4.put(
                "Estonia",
                new String[][]{{"Tallinn", "Jüri Ratas"}, {"Estonian", "Russian", "Finnish"}}
        );
        countries4.put(
                "Sweden",
                new String[][]{{"Stockholm", "Stean Lofven"}, {"Swedish", "Danish", "English", "German"}}
        );
        countries4.put(
                "Norway",
                new String[][]{{"Oslo", "Erna Solberg"}, {"Norwegian", "Swedish", "English"}}
        );

        for (String countryName : countries4.keySet()) {
            String countryCapital = countries4.get(countryName)[0][0];
            String countryPrimeMinister = countries4.get(countryName)[0][1];
            String[] countryLanguages = countries4.get(countryName)[1];

            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t\t" + countryLanguage);
            }

        }


        // Ex 11

        // Queue -> linkedList
        Queue<String[][]> countries5 = new LinkedList<>();
        countries5.add(new String[][]{{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Latvian", "Finnish"}});
        countries5.add(new String[][]{{"Germany"}, {"Berlin"}, {"Angela Merkel"}, {"German", "English"}});
        countries5.add(new String[][]{{"Finland"}, {"Helsinki"}, {"Sanna Marin"}, {"Finnish", "Estonian"}});


        // Variant 1 - kõigemõtekam vist
//        while(!countries5.isEmpty()) {
//            String[][] country = countries5.poll(); // poll k[sides eemaldab selle, remove ka
//            String countryName = country[0][0];
//            String countryCapital = country[1][0];
//            String countryPrimeMinister = country[2][0];
//            String[] countryLanguages = country[3];
//            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
//            for (String countryLanguage : countryLanguages) {
//                System.out.println("\t" + countryLanguage);
//            }
//            }

//        Variant 2

//         for (String[][] country : countries5) {
//            String countryName = country[0][0];
//            String countryCapital = country[1][0];
//            String countryPrimeMinister = country[2][0];
//            String[] countryLanguages = country[3];
//            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
//            for (String countryLanguage : countryLanguages) {
//                System.out.println("\t" + countryLanguage);
//            }
//         }

//        Variant 3
        for (String[][] country : countries5.toArray(String[][][]::new)) {
            String countryName = country[0][0];
            String countryCapital = country[1][0];
            String countryPrimeMinister = country[2][0];
            String[] countryLanguages = country[3];
            System.out.printf("%s / %s / %s:\n", countryName, countryCapital, countryPrimeMinister);
            for (String countryLanguage : countryLanguages) {
                System.out.println("\t" + countryLanguage);
            }
        }


    }
}
