package valiit.day05;

public class AdditionalExercises {
    public static void main(String[] args) {

        // Ex 1
        for (int row = 1; row<= 8; row++) {
            for (int col = 1; col <= 19; col++) {
                System.out.print((col + row) % 2 == 0 ? "#" : "+");
            }
            System.out.println();
        }

        // Ex 2

        for (int i = 1; i <=6; i++){
            for(int j = 1; j<i ; j++){
                System.out.print("-");
            }
            System.out.println("#");
        }

        // Ex 3

        for(int row = 1; row <= 5; row++){
            for(int col = 1; col <=7; col++){
                if(row % 2 == 0){
                    System.out.print("=");
                }else{
                    System.out.print(col % 3 == 1 ? "#" : "+");
                }
            }
            System.out.println();
        }

        // Ex 4

        String andmed = "Eesnimi: Teet, Perenimi: Kask, Vanus: 34, Amet: lendur, Kodakondsus: Eesti; Eesnimi: Mari, Perenimi: Tamm, Vanus: 56, Amet: kosmonaut, Kodakondsus: Soome; Eesnimi: Kalle, Perenimi: Kuul, Vanus: 38, Amet: arhitekt, Kodakondsus: Läti; Eesnimi: James, Perenimi: Cameron, Vanus: 56, Amet: riigiametnik, Kodakondsus: UK; Eesnimi: Donald, Perenimi: Trump, Vanus: 73, Amet: kinnisvaraarendaja, Kodakondsus: USA";
        String[] personsArray = andmed.split("; ");

        String[][] personDataArray = new String[personsArray.length][5];
        for (int i = 0; i < personsArray.length; i++) {
            String[] personArray = personsArray[i].split(", ");
            for (int j = 0; j < personArray.length; j++) {
                personArray[j] = personArray[j].split(": ")[1];
            }
            personDataArray[i] = personArray;
        }

        for (String[] person : personDataArray) {
            System.out.println("---------------------");
            System.out.printf("%s / %s / %s / %s / %s \n", person[0], person[1], person[2], person[3], person[4]);
        }

    }
}
