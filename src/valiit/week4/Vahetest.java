package valiit.week4;

import com.sun.jdi.IntegerValue;
import valiit.visitcounter.Visit;

import java.util.*;

public class Vahetest {

    public static void main(String[] args) {

        System.out.println("\nEx 1");
        int[] numbers = {3, 6, 1, 9, 45, 99};

        System.out.println("\nEx 2");
        int[] salaries = {1700, 1500, 2320, 980, 1320, 1500, 1550, 1790};
        System.out.println("Tulumaks: " + calculateTotalIncomeTax(salaries));

        System.out.println("\nEx 3");
        String text = "Meelas mõmm maiustas mäe otsas maasikatega";
        String text2 = "Kuri karu järas jõe ääres juurikaid.";
        System.out.println(countMs(text));
        System.out.println(countMs(text2));

        System.out.println("\nEx 4");

        SortedMap<String, String> countries = new TreeMap<>(
                new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s2.compareTo(s1);
            }
        });

        countries.put("83716", "Switzerland");
        countries.put("63987", "Singapore");
        countries.put("77771", "Ireland");
        countries.put("67037", "Iceland");
        countries.put("81151", "Macau");
        countries.put("59795", "Denmark");
        countries.put("69687", "Qatar");
        countries.put("65111", "United States");
        countries.put("77975", "Norway");

        for (String country : countries.keySet()) {
            System.out.println(String.format("%s \t %s", country, countries.get(country)));
        }

    }

    public static int calculateTotalIncomeTax(int[] pay) {
        int payTotal = 0;
        for (int s : pay) {
            payTotal += s;
        }

        payTotal = payTotal / 5;
        return payTotal;
    }

    public static int countMs(String mmm) {
        char[] mArray = String.format(mmm).toLowerCase().toCharArray();
        int i = 0;

        for (char j : mArray) {
            if (j == 'm') {
                i++;
            }
        }

        return i;
    }


}
