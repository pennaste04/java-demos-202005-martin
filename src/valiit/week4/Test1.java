package valiit.week4;

import java.util.Arrays;

public class Test1 {
    public static void main(String[] args) {
        int area = calcRectangleArea(5,6);
        System.out.println("Area:" + area);


        String[] words = {"Blabla", "1234", "kollane"};

        for(String w: censorWords(words)) {
            System.out.println(w);
        }

        String[] genres = {"Punk", "pop"};
        Song song = new Song("Kui meri oleks õlu",
                "tere", "kaks",
                1995, genres,
                "");





    }

    private static int calcRectangleArea(int sideA, int sideB) {
        return sideA * sideB;
    }


    private static String[] censorWords(String[] words) {
        String[] result = new String[words.length];

        for (int i = 0; i < words.length; i++) {
            result[i] = "#".repeat(words[i].length());
        }

        return result;
    }

    public static class Song { // siin static tähendab, et see on selle klassi alamklass, kui eraldi faili paneme, siis ei pane seda
        private String title;
        private String artist;
        private String album;
        private int released;
        private String[] genres;
        private String lyris;

        public Song(String title, String artist, String album, int released, String[] genres, String lyris) {
            this.title = title;
            this.artist = artist;
            this.album = album;
            this.released = released;
            this.genres = genres;
            this.lyris = lyris;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getArtist() {
            return artist;
        }

        public void setArtist(String artist) {
            this.artist = artist;
        }

        public String getAlbum() {
            return album;
        }

        public void setAlbum(String album) {
            this.album = album;
        }

        public int getReleased() {
            return released;
        }

        public void setReleased(int released) {
            this.released = released;
        }

        public String[] getGenres() {
            return genres;
        }

        public void setGenres(String[] genres) {
            this.genres = genres;
        }

        public String getLyris() {
            return lyris;
        }

        public void setLyris(String lyris) {
            this.lyris = lyris;
        }

        @Override
        public String toString() {
            return "Song{" +
                    "title='" + title + '\'' +
                    ", artist='" + artist + '\'' +
                    ", album='" + album + '\'' +
                    ", released=" + released +
                    ", genres=" + Arrays.toString(genres) +
                    ", lyris='" + lyris + '\'' +
                    '}';
        }
    }
}
