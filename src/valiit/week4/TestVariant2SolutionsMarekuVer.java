package valiit.week4;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestVariant2SolutionsMarekuVer {

    public static void main(String[] args) {
        System.out.println("Ex 4");

        List<Country> countries = new ArrayList<>();

        countries.add(new Country("Switzerland", 83716, 8654622));
        countries.add(new Country("Singapore", 63987, 5850342));
        countries.add(new Country("Ireland", 77771, 4937786));
        countries.add(new Country("Iceland", 67037, 341243));
        countries.add(new Country("Macau", 81151, 649335));
        countries.add(new Country("Denmark", 59795, 5792202));
        countries.add(new Country("Qatar", 69687, 2881053));
        countries.add(new Country("United States", 65111, 331002651));
        countries.add(new Country("Norway", 77975, 5421241));
        System.out.println(countries);

        Country biggestEconomy = null;

        findBiggestEconomy(countries, biggestEconomy);
        System.out.println("Suurim majandus on:");
        System.out.println(biggestEconomy);

//        Country smallestEconomy = countries.stream().min((c1, c2) - > {
//          if(c1.getTotalGdp() - c2.getTotalGdp() > 0) {
//              return 1;
//          } else if ()
//        })

//        Country smallestEconomy = countries.stream()
//                .min(Comparator.comparingDouble(Country::getTotalGdp)).get();

        Country smallestEconomy = countries.stream()
                .min((c1, c2) -> Double.compare(c1.getTotalGdp(), c2.getTotalGdp())).get();

        // Vaja Mareku oma vaadata
//        double averageEconomy = countries.stream().mapToDouble(c -> c.getTotalGdp()).average().orElse(0));
//        System.out.println("Keskmine majandus: %,.2f USD.", averageEconomy);

    }

    private static void findBiggestEconomy(List<Country> countries, Country biggestEconomy) {
        for (Country country : countries) {

            if (biggestEconomy == null) {
                biggestEconomy = country;
            } else if (country.getTotalGdp() > biggestEconomy.getTotalGdp()) {
                biggestEconomy = country;
            }
        }
    }

    public static class Country {
        private String name;
        private double gdpPerCapita;
        private double population;

        public Country(String name, double gdpPerCapita, double population) {
            this.name = name;
            this.gdpPerCapita = gdpPerCapita;
            this.population = population;
        }

        public String getName() {
            return name;
        }

        public double getGdpPerCapita() {
            return gdpPerCapita;
        }

        public double getPopulation() {
            return population;
        }

        public double getTotalGdp() {
            return population * gdpPerCapita;
        }

        @Override
        public String toString() {
            return "Country{" +
                    "name='" + name + '\'' +
                    ", gdpPerCapita=" + gdpPerCapita +
                    ", population=" + population +
                    ", totalGdp=" + getTotalGdp() +
                    '}';
        }
    }

}
