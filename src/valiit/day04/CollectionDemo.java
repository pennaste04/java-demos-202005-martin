package valiit.day04;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CollectionDemo {
    public static void main(String[] args) {

        // Set
        Set<String> namesSet = new HashSet<>();
        namesSet.add("Mari");
        namesSet.add("Malle");
        namesSet.add("Tõnu");
        namesSet.add("Abraham");

        // Prindi teine element
        String[] names = namesSet.toArray(String[]::new); // ::new - konstruktorfunktsioonipointer
        System.out.println(names[1]);

        // Kui tahan ükshaaval kõik seti elemendid läbi käia
        for(String setElement : namesSet) {
            System.out.println("FOR EACH ELEMENT: " + setElement);
        }

        // Map

//        Map<String, String> voroEstDictionary = new HashMap<>();
//        voroEstDictionary.put("putsunutsutaja", "tolmuimeja");
//        voroEstDictionary.put("rüpperaal", "sülearvuti");
//
//        System.out.println("Putsunutsutaja eesti keeles on: " + voroEstDictionary.get("putsunutsutaja"));
//        System.out.println("Kas putsunutsutaja on olemas? " + voroEstDictionary.containsKey("putsunutsutaja"));

//        voroEstDictionary.clear(); // puhastab kõik mapist

        Map<String, Integer> ages = new HashMap<>();
        ages.put("Mati", 25);
        ages.put("Kati", 30);
        ages.put("Tõnu", 35);
        ages.put("Tiit", 30);
        System.out.println(ages);
        System.out.println(ages.keySet());

        for (String name : ages.keySet()) {
            System.out.println(String.format("%s on %d aastat vana.", name, ages.get(name)));
        }

        System.out.println("values() funktsioon...");
        System.out.println(ages.values());

        System.out.println("entrySet() funktsioon...");
        System.out.println(ages.entrySet());

        for(Map.Entry<String, Integer> entry: ages.entrySet()) {
            System.out.println("Entry...");
            System.out.println(entry);
        }

    }
}
