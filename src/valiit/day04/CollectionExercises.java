package valiit.day04;

import java.util.*;
import java.util.stream.Stream;

public class CollectionExercises {
    public static void main(String[] args) {

        // ex 19
//        List<String> cityName = new ArrayList<>();

//        cityName.add("Narva");
//        cityName.add("Tallinn");
//        cityName.add("Tartu");
//        cityName.add("Keila");
//        cityName.add("Kuressaare");

        // Järgnev on immutable list ehk muutmatu kui niiviisi lisada
        // List<String> cities = Arrays.asList("Narva", "Tallinn", "Tartu", "Keila", "Kuressaare");

        //
        List<String> cityName = new ArrayList<>(Arrays.asList("Narva", "Tallinn", "Tartu", "Keila", "Kuressaare"));

        cityName.add("Baden-Baden");

        System.out.println("Esimene linn: " + cityName.get(0));
        System.out.println("Esimene linn: " + cityName.get(2));
        System.out.println("Esimene linn: " + cityName.get(cityName.size() - 1));


        // Ex 20
        Queue<String> namesQueue = new LinkedList<>();
        namesQueue.add("Mari");
        namesQueue.add("Teet");
        namesQueue.add("Toomas");
        namesQueue.add("Adalbert");
        namesQueue.add("Kairi");
        namesQueue.add("Nele");
        System.out.println(namesQueue.poll()); // kustutab esimese ära (Mari)
        System.out.println(namesQueue.poll()); // kustutab esimese ära (Teet)
        System.out.println(namesQueue.poll()); //
        System.out.println(namesQueue.poll()); //
        System.out.println(namesQueue.poll()); //
        System.out.println(namesQueue.poll()); //
        while (!namesQueue.isEmpty()) { // kontrollib, ega pole tühi sama kui namesQueue.peek() != null või namesQueue.size() > 0
            System.out.println(namesQueue.remove()); // sama kui poll, aga tekitab errori kui enam pole midagi ära eemaldada
        }

        // Ex 21

        Set<String> asjad = new TreeSet<>();
        asjad.add("üks");
        asjad.add("kaks");
        asjad.add("kolm");
        asjad.add("neli");

//        for (String tekst : asjad) {
//            System.out.println(tekst);
//        }

//        asjad.forEach(tekst -> System.out.println(tekst));
        asjad.forEach(System.out::println); // soovituslik see variant

        // Ex 21

        Map<String, String[]> riigiLinnad = new HashMap<>();
        String[] estoniaCities = {"Tallinn", "Tartu", "Valga", "Võru"};
        riigiLinnad.put("Estonia", estoniaCities);

        String[] swedenCities = {"Stockholm", "Uppsala", "Lund", "Köping"};
        riigiLinnad.put("Sweden", swedenCities);

        String[] finlandCities = {"Helsinki", "Espoo", "Hanko", "Jämsä"};
        riigiLinnad.put("Finland", finlandCities);

        riigiLinnad.put("Finland", new String[]{"Helsinki", "Espoo", "Hanko", "Jämsä"});

        System.out.println(riigiLinnad);


        Map<String, List<String>> countries = new HashMap<>();
//        countries.put("Estonia", Arrays.asList("Tallinn", "Tartu", "Valga", "Võru"));
//        countries.put("Sweden", Arrays.asList("Stockholm", "Uppsala", "Lund", "Köping"));
//        countries.put("Finland", Arrays.asList("Helsinki", "Espoo", "Hanko", "Jämsä"));

//        for (String country : countries.keySet()) {
//            System.out.println("Country: " + country);
//            System.out.println("Cities:");
//            for (String city : countries.get(country)) {
//                System.out.println("\t" + city);
//            }
//        }

        riigiLinnad.forEach((country, countryCities) -> {
            System.out.println("Country: " + country);
            System.out.println("Cities:");
//            Stream.of(countryCities).forEach(countryCityName -> System.out.println("\t\t\t" + countryCityName));
        // Massiv listi kujule näide
            Arrays.asList(countryCities).stream().forEach(countryCity -> System.out.println("\t " + countryCity));

        });




    }
}
