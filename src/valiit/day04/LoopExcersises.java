package valiit.day04;

public class LoopExcersises {
    public static void main(String[] args) {

        // ex 11
        int a = 1;
        while (a <= 100) {
            System.out.println("Rida " + a);
            a++;
        }

        // ex 12
        for (int f = 1; f <= 100; f++) {
            System.out.println("Rida " + f);
        }

        // ex 13
        int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int e : numbers) {
            System.out.println(e);
        }

        // ex 14
        for (int j = 3; j < 100; j = j + 3) {
            System.out.println(j);
        }

        // ex 15
        String muusika[] = {"Sun", "Metsatöll", "Queen", "Metallica"};
        for (int i = 0; i < muusika.length; i++) {
            if (muusika.length != i + 1) {
                System.out.print(muusika[i] + ", ");
            } else {
                System.out.print(muusika[i]);
            }
        }
        System.out.println("\n");

        // Additional possibility example for ex 15
        String bandsTextWithJoin = String.join(", ", muusika);
        System.out.println(bandsTextWithJoin);


        // ex 16
        String muusika2[] = {"Sun", "Metsatöll", "Queen", "Metallica"};
        for (int i = muusika2.length; i > 0; i--) {
            if (i > 1) {
                System.out.print(muusika2[i - 1] + ", ");
            } else {
                System.out.print(muusika2[i - 1]);
            }
        }

        System.out.println("\n");

        // ex 17
        String[] numberToText = {"null", "üks", "kaks", "kolm", "neli", "viis",
                "kuus", "seitse", "kaheksa", "üheksa"};
        String numbersText = "";
        int aa = 0;
        for (String x : args) {
            int z = Integer.valueOf(x); // või int z = Integer.parseInt(args[x]);
//            if(aa < args.length-1) {
//                System.out.print(numberToText[z] + ", ");
//            }else{
//                System.out.print(numberToText[z] + ".");
//            }
            numbersText = numbersText + ", " + numberToText[z];
            aa++;
        }
        numbersText = numbersText.length() >= 2 ? numbersText.substring(2) : "";
        System.out.println(numbersText);

        // ex 18

        double randomNum;
        do {
            System.out.println("tere ");
            randomNum = Math.random();
        } while(randomNum < 0.5);


    }
}
