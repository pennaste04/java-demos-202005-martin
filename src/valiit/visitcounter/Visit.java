package valiit.visitcounter;

public class Visit implements Comparable<Visit> {
    private String date;
    private int count;

    public Visit(String visitString) {
        // "2018-01-03, 731" --> date: "2018-01-03"; count: 731;
        String[] props = visitString.split(", ");
        this.date = props[0];
        this.count = Integer.parseInt(props[1]);
    }

    public String getDate() {
        return date;
    }

    public int getCount() {
        return count;
    }

    @Override
    public String toString() {
        return "Visit{" +
                "date='" + date + '\'' +
                ", count=" + count +
                '}';
    }

    @Override
    public int compareTo(Visit otherVisit) {
        return this.getCount() - otherVisit.getCount();
    }
}
