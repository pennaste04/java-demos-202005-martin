package valiit.visitcounter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CounterApp {
    public static void main(String[] args) {
        List<Visit> visits = loadVisitsFromFile("resources/visits.txt");

        // Variant 1: Comparator
//        visits.sort((v1, v2) -> v1.getCount() - v2.getCount());
//        Collections.sort(visits, (v1, v2) -> v1.getCount() - v2.getCount());

        // Variant 2: Comparable
        Collections.sort(visits);

        for (Visit visit : visits) {
            System.out.println("----------------------------");
            System.out.println("Kuupäev:     " + visit.getDate());
            System.out.println("Külastajaid: " + visit.getCount());
        }

        System.out.println("----------------------------");
        Visit lastVisit = visits.get(visits.size() - 1);
        System.out.printf("Kõige rohkem külastajaid oli %s (%d inimest).", lastVisit.getDate(), lastVisit.getCount());

    }

    private static Visit getMaxVisitEntity(List<Visit> visits) {
        // Variant 1 - klassikaline
//        Visit max = null;
//        for (Visit visit : visits) {
//            if (max == null) {
//                max = visit;
//            } else if (visit.getCount() > max.getCount()) {
//                max = visit;
//            }
//        }
//        return max;

        // Variant 2 - Lambda
        return visits.stream().max((v1, v2) -> v1.getCount() - v2.getCount()).orElse(null);
    }

    private static List<Visit> loadVisitsFromFile(String filePath) {
        try {
            List<String> fileLines = Files.readAllLines(Paths.get(filePath));
            // Konverteerime stringi Visit-objektideks...
            List<Visit> visits = new ArrayList<>();

            // Variant 1: klassika
//            for (String visitString : fileLines) {
//                visits.add(new Visit(visitString));
//            }

            // Variant 2: streamid
            visits = fileLines.stream().map(visitString -> new Visit(visitString)).collect(Collectors.toList());

            return visits;
        } catch (IOException e) {
            System.out.println("Mingi jama...");
            e.printStackTrace();
            return null;
        }
    }
}
