package valiit.day07;

public class Human {
    private String firstName = "John";

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
}
