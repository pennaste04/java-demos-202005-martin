package valiit.day07;

public class DogManager {
    public static void main(String[] args) {
        System.out.println("Greek dog");
        Dog greekDog = new GreekDog("Aphrodite");
        greekDog.bark();

        System.out.println("Serbian dog");
        Dog serbianDog = new SerbianDog("Sarko");
        serbianDog.bark();

        System.out.println("Latvian dog");
        Dog latvianDog = new LatvianDog("Rufo");
        latvianDog.bark();

    }
}
