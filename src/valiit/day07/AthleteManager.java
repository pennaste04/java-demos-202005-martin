package valiit.day07;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class AthleteManager {
    public static void main(String[] args) {
        // instantsialiseerimine == objekti loomine
//        Athlete athlete1 = new Athlete(); // Vaatleme seda kui atleeti
//        Object athlete2 = new Athlete(); // Vaatleme seda kui objekti
//
//        ((Athlete)athlete2).firstName
//
//        // Polümorfism
//        Object skydiver1 = new Skydiver();
//        Athlete skydiver2 = new Skydiver();
//        Skydiver skydiver3 = new Skydiver();
//
//        skydiver3.perform();
//
//        ArrayList<String> list = new ArrayList<>();
//        Set<String> set = new HashSet<>();

        Athlete skydiver = new Skydiver("Tanel", "Padar", 35, "M", 187, 85);
        Athlete runner = new Runner("Heidi", "Purga", 40, "F", 168, 65, 35);

        skydiver.perform();
        runner.perform();

        System.out.println(skydiver.getFirstName());

        Human h1 = new Human();
        h1.setFirstName("Mario");
        h1.setFirstName("Mario");
        h1.setFirstName("Mario");
        System.out.println(h1.getFirstName());

    }
}
