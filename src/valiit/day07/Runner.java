package valiit.day07;

public class Runner extends Athlete {
    public double maxSpeed;

    public Runner(String firstName, String lastName, int age, String gender, double height, double weight, double maxSpeed) {
        super(firstName, lastName, age, gender, height, weight); // Peab olema esimene statement!
        this.maxSpeed = maxSpeed;
    }

    @Override
    public void perform() {
        System.out.println("Rushing like crazy...");
    }
}
