package valiit.day07;

public class LatvianDog extends Dog {

    public LatvianDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("blabla latvian");
    }
}
