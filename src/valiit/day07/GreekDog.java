package valiit.day07;

public class GreekDog extends Dog {

    public GreekDog(String name) {
        super(name);
    }

    @Override
    public void bark() {
        System.out.println("ghav-ghav");
    }
}
