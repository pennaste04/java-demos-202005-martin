package valiit.bank;

import java.io.IOException;
import java.util.Scanner;
import java.util.function.DoubleUnaryOperator;

public class BankApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        try {
            AccountService.loadAccounts("resources/kontod.txt");
        } catch (IOException e) {
            System.out.println("Ei leidnud kahjuks faili, mida lugeda :(");
            return;
//            e.printStackTrace();
        }

        System.out.println("----------------------");
        System.out.println("-------- PANK --------");
        System.out.println("----------------------");

        System.out.println("Variandid:");
        System.out.println("EXIT");
        System.out.println("BALANCE <ACCOUNT>");
        System.out.println("BALANCE <FIRST NAME> <LAST NAME>");
        System.out.println("TRANSFER <FROM ACCOUNT> <TO ACCOUNT> SUM");

        while (true) {
            System.out.println("Sisesta käsk:");
            String input = scanner.nextLine();

            String[] inputParts = input.split(" ");

            // EXIT
            if (inputParts[0].equalsIgnoreCase("EXIT")) {
                System.out.println("Paka paka!");
                return;
            }else if ("BALANCE".equalsIgnoreCase(inputParts[0])) {
                Account account = null;
                if (inputParts.length == 2) {
                    account = AccountService.findAccount(inputParts[1]);
                } else if (inputParts.length == 3) {
                    account = AccountService.findAccount(inputParts[1], inputParts[2]);
                }
                displayAccountDetails(account);
            }else if("TRANSFER".equalsIgnoreCase(inputParts[0])) {
                if(inputParts.length < 4) {
                    System.out.println("Ebakorrektsed lähteandmed, ei saa ülekannet teostada.");
                }else {
                    double sum = getSumFromText(inputParts[3]);
                    TransferResult result = AccountService.transfer(inputParts[1], inputParts[2], sum);
                    displayTransferResult(result);
                }
            }
            else{
                System.out.println("Tundmatu käsk!");
            }
            System.out.println("----------------------");
        }
    }

    private static void displayTransferResult(TransferResult result) {
        if (result.isSuccess()) {
            System.out.println("----------------------");
            System.out.println("ÜLEKANNE ÕNNESTUS");
            System.out.println("MAKSJA:");
            displayAccountDetails(result.getFromAccount());
            System.out.println("SAAJA:");
            displayAccountDetails(result.getToAccount());
        } else {
            System.out.println("----------------------");
            System.out.println("ÜLEKANNE EBAÕNNESTUS");
            System.out.println("PÕHJUS: " + result.getMessage());
        }
    }

    private static double getSumFromText(String inputPart) {
        try {
            return Double.parseDouble(inputPart);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    private static void displayAccountDetails(Account account) {
        if (account != null) {
            System.out.println("----------------------");
            System.out.printf("NIMI: \t\t%s %s\n", account.getFirstName(), account.getLastName());
            System.out.printf("KONTO: \t\t%s\n", account.getAccountNumber());
            System.out.printf("JÄÄK: \t\t%s\n", account.getBalance());
        } else {
            System.out.println("Kontot ei leitud");
            System.out.println("----------------------");
        }
    }
}
