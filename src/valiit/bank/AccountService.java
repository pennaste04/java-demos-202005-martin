package valiit.bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class AccountService {

    public static List<Account> getAccounts() {
        return accounts;
    }

    private static List<Account> accounts = new ArrayList<>();

    public static void loadAccounts(String accountsFilePath) throws IOException {
        List<String> accountCsvs = Files.readAllLines(Paths.get(accountsFilePath));

        // Tekitame konto objektid ja paneme need listi.


//        for (String accountCsv : accountCsvs) {
//            Account acc = new Account(accountCsv);
//            accounts.add(acc);
//        }
        accounts = accountCsvs.stream().map(Account::new).collect(Collectors.toList());

    }


    public static Account findAccount(String accountNumber) {
        for (Account account : accounts) {
            if (account.getAccountNumber().equals(accountNumber)) {
                return account;
            }
        }
        return null;

//        // Teine võimalus
//        return accounts
//                .stream().filter(a -> a.getAccountNumber().equals(accountNumber))
//                .findFirst().orElse(null);
//
//        // Kolmas võimalus
//        List<Account> filteredAccounts = accounts
//                .stream().filter(acc -> acc.getAccountNumber().equals(accountNumber))
//                .collect(Collectors.toList());
//        return filteredAccounts.size() > 0 ? filteredAccounts.get(0) : null;
    }

    public static Account findAccount(String firstName, String lastName) {
        for (Account account : accounts) {
            if (account.getFirstName().equalsIgnoreCase(firstName) &&
                    account.getLastName().equalsIgnoreCase(lastName)) {
                return account;
            }
        }
        return null;

        // Teine võimalus
//        return accounts.stream()
//                .filter(account -> account.getFirstName().equalsIgnoreCase(firstName) &&
//                        account.getLastName().equalsIgnoreCase(lastName))
//                .findFirst().orElse(null);
    }


    public static TransferResult transfer(String fromAccountNumber, String toAccountNumber, double sum) {
        Account from = findAccount(fromAccountNumber);
        Account to = findAccount(toAccountNumber);

        // Valideerimine (kas saab ülekannet teostada?)

        if (from == null) {
            return new TransferResult(false, "Payer account does not exist.");
        } else if (to == null) {
            return new TransferResult(false, "Beneficiary account does not exist.");
        } else if (!(sum > 0)) {
            return new TransferResult(false, "Amount has to be higher than 0");
        } else if (from.getBalance() < sum) {
            return new TransferResult(false, "Payer has insufficient funds");
        }


        from.setBalance(from.getBalance() - sum);
        to.setBalance(to.getBalance() + sum);
        return new TransferResult(true, "OK", from, to);
    }

}
