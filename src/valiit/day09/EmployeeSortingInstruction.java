package valiit.day09;

import java.util.Comparator;

public class EmployeeSortingInstruction implements Comparator<Employee> {
    @Override
    public int compare(Employee employee1, Employee employee2) {

        if(employee1.getSalary() - employee2.getSalary() != 0) {
            return employee1.getSalary() - employee2.getSalary();
        }
        if(employee1.getFirstName().compareTo(employee2.getFirstName()) != 0){
            return employee1.getFirstName().compareTo(employee2.getFirstName());
        } else {
            return employee1.getLastName().compareTo(employee2.getLastName());
        }

    }
}
