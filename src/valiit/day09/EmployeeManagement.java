package valiit.day09;

import java.util.*;

public class EmployeeManagement {
    public static void main(String[] args) {
        Employee mati = new Employee("Mati", "Kask",1500, "Eesti");
        Employee kati = new Employee("Kati", "Tamm", 1800, "Rootslane");
        Employee priit = new Employee("Priit", "Järv", 1300, "Soomlane");
        Employee adalbert = new Employee("Adalbert", "Mägi", 2000, "Sakslane");
        Employee adalbert2 = new Employee("Adalbert", "Mägi", 1100, "Lätlane");

//        System.out.println(adalbert.equals(adalbert2));
//        Set<Employee> employeeSet = new HashSet<>();
//        employeeSet.add(mati);
//        employeeSet.add(kati);
//        employeeSet.add(priit);
//        employeeSet.add(adalbert);
//        employeeSet.add(adalbert2);
//        System.out.println(employeeSet);


        List<Employee> employees = new ArrayList<>();
        employees.add(mati);
        employees.add(kati);
        employees.add(priit);
        employees.add(adalbert);
        employees.add(adalbert2);
        // Variant 1 - klassikaline lahendus
//        Collections.sort(employees, new EmployeeSortingInstruction());

        // Variant 2 - interface inline-implementatsioon (pool-modernne)
//        Comparator<Employee> empComparator = new Comparator<Employee>() {
//            @Override
//            public int compare(Employee employee1, Employee employee2) {
//                return employee2.getSalary() - employee1.getSalary();
//            }
//        };

        // Variant 3 - Cool, äge, lahe, modernne! (lambda avaldis)
        Collections.sort(employees, (employee1, employee2) -> employee1.getSalary() - employee2.getSalary());

        // Lambda avaldis on interface'i inline-realiseering kompaktses vormis
        // Lambda avaldist saab kasutada ainult siis, kui interface on @FunctionalInterface
        // Mis on functional intergace - see on interface, millel on ainult üks implementeerimist vajav meetod.

        //Variant 4 - Supecool! (Aga megakeeruline)
//        Collections.sort(employees, Comparator.comparingInt(Employee::getSalary));

        System.out.println(employees);

//        System.out.println(adalbert.getFirstName().equals(adalbert2.getFirstName())); // nii saab võrrelda
//        System.out.println(adalbert == adalbert2); // false, nii ei saa võrrelda kui just adalbert2 = adalbert1 ei ole kirjas

//        List<Employee> employees = new ArrayList<>();
//        employees.add(mati);
//        employees.add(kati);
//        employees.add(priit);
//        employees.add(adalbert);
//        employees.add(adalbert2);
//        System.out.println(employees);
    }
}
