package valiit.day06;

import java.util.Arrays;

public class CountryApp {
    public static void main(String[] args) {

        CountryInfo estonia = new CountryInfo("Eesti", "Tallinn", "Jüri Ratas", new String[]{"Eesti", "Vene", "Läti", "Soome"});

        CountryInfo latvia = new CountryInfo("Latvia", "Riga", "Arturs Krisjanis Karins", new String[]{"Läti", "Vene", "Eesti", "Leedu"});

        CountryInfo lithuania = new CountryInfo();
        lithuania.countryName = "Lithuania";
        lithuania.countryCapital = "Vilnius";
        lithuania.countryPrimeMinister = "Saulis Skvernelis";
        lithuania.countryLanguages = new String[]{"Leedu", "Vene", "Läti", "Poola"};

//        {{"Estonia"}, {"Tallinn"}, {"Jüri Ratas"}, {"Estonian", "Latvian", "Finnish"}},
//        {{"Latvia"}, {"Riga"}, {"Arturs Krišjānis Kariņš"}, {"Latvian", "Russian", "Estonian"}},
//        {{"Lithuania}"}, {"Vilnius"}, {"Saulius Skvernelis"}, {"Lithuanian", "Russian"}},

        CountryInfo[] balticStates = new CountryInfo[3];
        balticStates[0] = estonia;
        balticStates[1] = latvia;
        balticStates[2] = lithuania;

        System.out.println(Arrays.toString(balticStates));
        System.out.println("Loodud riikide arv: " + CountryInfo.countryCount);
    }
}
