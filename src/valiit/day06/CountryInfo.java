package valiit.day06;

import java.util.Arrays;

public class CountryInfo {

    public String countryName;
    public String countryCapital;
    public String countryPrimeMinister;
    public String[] countryLanguages;

    public static int countryCount = 0;

    public CountryInfo(String countryName, String countryCapital, String countryPrimeMinister, String[] countryLanguages) {
        this.countryName = countryName;
        this.countryCapital = countryCapital;
        this.countryPrimeMinister = countryPrimeMinister;
        this.countryLanguages = countryLanguages;
        CountryInfo.countryCount++;
    }

    public CountryInfo() {
        CountryInfo.countryCount++;
    }

    @Override
    public String toString() {
        return "{" +
                "='" + countryName + '\'' +
                ", '" + countryCapital + '\'' +
                ", '" + countryPrimeMinister + '\'' +
                ", " + Arrays.toString(countryLanguages) +
                '}';
    }
}
