package valiit.day06;

public class Pet {
    // Need muutujad ei kuulu klassile.
    // Need kuuluvad selle klassi baasil loodud objektile.
    public String name;
    public int ageInMonths;
    public String color;
    public double weight;
    public String description;
    public boolean isVaccinated;

    // See muutuja kuulub klassile, seda ei panda objekti sisse.
    public static int petCount = 0;

    // this - see objekt mille sisse ma kuulun!!!
    public void introduce() {
        System.out.println("Mina olen: " + this.name);
    }


}
