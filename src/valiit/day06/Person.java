package valiit.day06;

public class Person {
    public String personalCode;

    public Person(){

    }

    public Person(String personalCode){ // Hea tava näeb ette, et konstruktorfunktsiooni sisendparameetrid on samad ja sama nimega mis objektil endal
        System.out.println("Konstruktor käivitus...");
        this.personalCode = personalCode;
    }

    public String getGender() {
        int sugu = Integer.parseInt(this.personalCode.substring(0, 1));

//        if(sugu % 2 == 0) {
//            return "F";
//        }else{
//            return "M";
//        }
        return sugu % 2 == 0 ? "F" : "M";

    }

    public int getBirthYear() {


        // .substring()
        // Integer.parseInt()

        // Valideerime
        if (this.personalCode == null) {
            return -1;
        } else if (this.personalCode.length() != 11) {
            return -1;
        }

        int century = Integer.parseInt(this.personalCode.substring(0, 1)); // 4
        int birthYear = Integer.parseInt(this.personalCode.substring(1, 3)); // 94 (ühest kolmeni, kolm pole kaasa arvatud)

        switch (century) {
            case 1:
            case 2:
                return 1800 + birthYear;
            case 3:
            case 4:
                return 1900 + birthYear;
            case 5:
            case 6:
                return 2000 + birthYear;
            case 7:
            case 8:
                return 2100 + birthYear;
            default:
                return 0;
        }

    }

    public String getBirthMonth() {
        int birthMonth = Integer.parseInt(this.personalCode.substring(3, 5));
        String[] monthName = {"Jaanuar", "Veebruar", "Märts", "Aprill", "Mai", "Juuni", "Juuli",
                "August", "September", "Oktoober", "November", "Detsember"};

        return monthName[birthMonth-1];
    }

    public int getDayOfMonth() {
        return Integer.parseInt(this.personalCode.substring(5, 7));
    }
}
