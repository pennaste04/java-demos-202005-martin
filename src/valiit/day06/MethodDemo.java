package valiit.day06;

public class MethodDemo {

    public static void main(String[] args) {
        int myCalculatedStuff = doSomePointlessCalculation(1, 2, 3);
        System.out.println(myCalculatedStuff);
        printGreetingToConsole();
        printAdvancedGreeting("Matthew");
    }

    // Funktsiooni defineerimine
    private static int  doSomePointlessCalculation(int number1, int number2, int number3) {

        int result = (number1 + number2 + number3) / 2;
        return result;

    }

    // Ilma tagastamiseta funktsiooni defineerimine

    private static void printGreetingToConsole() {
        System.out.println("Hello World!");
    }

    private static void printAdvancedGreeting(String name) {
        if (name.equals("Matthew")) {
            return; // Matthew'd ei tervitata.
        }
        System.out.println("Hello " + name + "!");
    }

}
