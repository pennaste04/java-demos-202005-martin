package valiit.day06;

import java.util.ArrayList;
import java.util.Arrays;

public class MethodExercises {
    public static void main(String[] args) {
        String personalCode1 = "49403136526";
//        int birthYear1 = retrieveBirthYear(personalCode1);
//        System.out.printf("Isikukood: %s, sünniaasta: %s", personalCode1, birthYear1);


        test2("tere", "head aega");

        System.out.println(addVat(100.0));
        System.out.println(Arrays.toString(composeArray(100, 2, true)));

        printTere();
        System.out.println(deriveGender(personalCode1));



    }


    // Ex 1

//    public static int retrieveBirthYear(String personalCode) { //49403136526
//
//        // .substring()
//        // Integer.parseInt()
//
//        // Valideerime
//        if (personalCode == null) {
//            return -1;
//        }else if(personalCode.length() != 11) {
//            return -1;
//        }
//
//        int century = Integer.parseInt(personalCode.substring(0, 1)); // 4
//        int birthYear = Integer.parseInt(personalCode.substring(1, 3)); // 94 (ühest kolmeni, kolm pole kaasa arvatud)
//
//        switch (century) {
//            case 1:
//            case 2:
//                return 1800 + birthYear;
//            case 3:
//            case 4:
//                return 1900 + birthYear;
//            case 5:
//            case 6:
//                return 2000 + birthYear;
//            case 7:
//            case 8:
//                return 2100 + birthYear;
//
//        }
//
//
//    }

    private static void test2(String text1, String text2) {

    }

    // Ex 3

    private static double addVat(double netPrice) {
        return 1.2 * netPrice;
    }

    // Ex 4

    private static int[] composeArray(int a, int b, boolean duplicate) {
        if (duplicate) {
            return new int[]{a * 2, b * 2};
        }
        return new int[]{a, b};

    }

    // Ex 5
    private static void printTere() {
        System.out.println("Tere");
    }

    private static String deriveGender(String isikukood) {
        int sugu = Integer.parseInt(isikukood.substring(0, 1));

//        if(sugu % 2 == 0) {
//            return "F";
//        }else{
//            return "M";
//        }
        return sugu % 2 == 0 ? "F" : "M";

    }

    private static boolean validatePersonalCode(String personalCode) { // 34501234215

        if (personalCode == null) {
            return false;
        } else if (personalCode.length() != 11) {
            return false;
        }

        String personalCodeParts[] = personalCode.split("");
        int[] personalCodeDigits = new int[11];
        for (int i = 0; i < 11; i++) {
            personalCodeDigits[i] = Integer.parseInt(personalCodeParts[i]);
        }

        int[] weights1 = {1, 2, 3, 4, 5, 6, 7, 8, 9, 1};
        int sum = 0;

        for(int i = 0; i < 10; i++){
            sum = sum + weights1[i] * personalCodeDigits[i];
        }

        int checkNumber = sum % 11;

        if (checkNumber != 10) {
            return personalCodeDigits[10] == checkNumber;
        }

        int [] weights2 = {3,4,5,6,7,8,9,1,2,3};
        sum = 0;
        for(int i = 0; i < 10; i++){
            sum = sum + weights2[i] * personalCodeDigits[i];
        }

        checkNumber = sum % 11;

        if (checkNumber !=  10) {
            return personalCodeDigits[10] == checkNumber;
        }else{
            return personalCodeDigits[10] == checkNumber;
        }

    }

}
